FROM node:lts-alpine

WORKDIR /usr/src/app
ENV PATH="/usr/src/app:${PATH}"

RUN apk add --no-cache jq curl

COPY src/package*.json ./
COPY src .

RUN npm ci --only=production

CMD [ "cli.js"]