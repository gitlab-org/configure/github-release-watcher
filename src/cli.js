#!/usr/bin/env node
const { Command } = require('commander');
const getReleasesMessage = require('./index').getReleasesMessage
const program = new Command();

async function main() {
  program
    .name('github-release-watcher')
    .description('CLI to check for new releases on GitHub')
    .option('-t, --time-passed <number>', 'How new should a release be to be included in the report? Specify in days.', 1)
    .option('-j, --json', 'JSON output')
    .option('-p, --no-pre-off', 'Filter out pre-releases')
    .argument('repos-list,', 'A comma-separated list of GitHub repo slugs.')
    .action(getReleasesMessage)
    // .version('0.8.0')
  await program.parseAsync(process.argv)
}

main().then(() => {
  console.error("Done")
})