const proxyquire = require('proxyquire')
const should = require('should')

const main = proxyquire('../index', {
  './releases': async function(repo) {
    if(repo.startsWith('hasnew/')) {
      return {
        repo, 
        name: 'New release',
        published_at: 'now',
        html_url: 'here'
      }
    } else {
      return null
    }
  }
})

describe('Main', () => {
  describe('getNewReleaseData', () => {
    it('returns a list by default', async () => {
      const newData = await main.getNewReleaseData([], 1)
      should(newData).be.eql([])
    })
    it('returns a list of new releases', async () => {
      const newData = await main.getNewReleaseData(['hasnew/first', 'old/old', 'hasnew/second'], 1)
      newData.should.have.length(2)
    })
  })
  describe('getReleasesMessage', () => {
    it('returns an empty message if no new release is available', async () => {
      const newData = await main.getReleasesMessage('valami/barmi, ize/bize', {timePassed: 1})
      should(newData).be.eql('')
    })
    it('returns a text with new releases', async () => {
      const newData = await main.getReleasesMessage('hasnew/first, ize/bize', {timePassed: 1})
      should(newData).not.be.eql('')
    })
    it('returns the json if requested', async () => {
      const newData = await main.getReleasesMessage('barmi/valami', {timePassed: 1, json: true})
      should(newData).be.eql('[]')
    })
  })

  describe('createMessage', () => {

    it('returns an empty string if releases array is empty', () => {
      const releases = []
      should(main.createMessage(releases)).be.eql('') 
    })

    it('returns a message for each release', () => {
      const releases = [
        {
          repo: 'repo1', 
          name: 'v1.0.0',
          published_at: '2020-01-01',
          html_url: 'https://github.com/user/repo1/releases/tag/v1.0.0'  
        },
        {
          repo: 'repo2',
          name: 'v2.0.0',
          published_at: '2020-02-01',
          html_url: 'https://github.com/user/repo2/releases/tag/v2.0.0'
        }
      ]

      const expected = `There is a new release in repo1: v1.0.0. It was published on 2020-01-01. For more info visit https://github.com/user/repo1/releases/tag/v1.0.0

There is a new release in repo2: v2.0.0. It was published on 2020-02-01. For more info visit https://github.com/user/repo2/releases/tag/v2.0.0`

      should(main.createMessage(releases)).be.eql(expected)
    })

    it('formats the message correctly', () => {
      const release = {
        repo: 'test-repo',
        name: 'v1.2.3', 
        published_at: '2022-01-15T12:00:00Z',
        html_url: 'https://github.com/user/test-repo/releases/tag/v1.2.3'
      }

      const expected = `There is a new release in test-repo: v1.2.3. It was published on 2022-01-15T12:00:00Z. For more info visit https://github.com/user/test-repo/releases/tag/v1.2.3`

      should(main.createMessage([release])).be.eql(expected)
    })

  })
})